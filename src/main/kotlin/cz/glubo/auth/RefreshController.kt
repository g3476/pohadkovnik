package cz.glubo.auth

import cz.glubo.FlashMessage
import cz.glubo.FlashMessage.Type.INFO
import cz.glubo.FlashMessage.Type.WARNING
import cz.glubo.auth.IdTokenLoginHandlerImpl.Companion.COOKIE_NAME_REFRESH_TOKEN
import cz.glubo.layout
import io.github.oshai.kotlinlogging.KotlinLogging
import io.micronaut.context.BeanProvider
import io.micronaut.core.type.Argument
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Produces
import io.micronaut.http.cookie.Cookie
import io.micronaut.security.annotation.Secured
import io.micronaut.security.authentication.Authentication
import io.micronaut.security.oauth2.client.OpenIdProviderMetadata
import io.micronaut.security.oauth2.configuration.OauthClientConfiguration
import io.micronaut.security.oauth2.endpoint.DefaultSecureEndpoint
import io.micronaut.security.oauth2.endpoint.SecureEndpoint
import io.micronaut.security.oauth2.endpoint.token.request.TokenEndpointClient
import io.micronaut.security.oauth2.endpoint.token.request.context.AbstractTokenRequestContext
import io.micronaut.security.oauth2.endpoint.token.response.OpenIdTokenResponse
import io.micronaut.security.oauth2.endpoint.token.response.TokenErrorResponse
import io.micronaut.security.oauth2.grants.AbstractClientSecureGrant
import io.micronaut.security.oauth2.grants.RefreshTokenGrant
import io.micronaut.security.rules.SecurityRule
import kotlinx.coroutines.reactive.awaitSingle
import java.time.OffsetDateTime
import kotlin.jvm.optionals.getOrNull

@Controller
class RefreshController(
    private val tokenEndpointClient: TokenEndpointClient,
    private val oauthClientConfiguration: OauthClientConfiguration,
    private val openIdProviderMetadata: BeanProvider<OpenIdProviderMetadata>,
) {
    private val logger = KotlinLogging.logger { }

    @Secured(SecurityRule.IS_ANONYMOUS)
    @Get("/refresh")
    @Produces(MediaType.TEXT_HTML)
    suspend fun refresh(
        authentication: Authentication?,
        request: HttpRequest<Any>,
    ): HttpResponse<String> {
//        val originalUri =
//            request.cookies
//                .findCookie("ORIGINAL_URI")
//                .getOrNull()
//                ?.value
        logger.info {
            request.cookies.all.map {
                "${it.name}: ${it.value}"
            }
        }
        val refreshToken =
            request.cookies
                .findCookie(COOKIE_NAME_REFRESH_TOKEN)
                .getOrNull()
                ?.value
        val tokenResponse =
            refreshToken ?.let {
                try {
                    tokenEndpointClient
                        .sendRequest(
                            RefreshTokenRequestContext(
                                refreshToken,
                                "",
                                getTokenEndpoint(),
                                oauthClientConfiguration,
                            ),
                        ).awaitSingle()
                } catch (e: Throwable) {
                    logger.warn { e }
                    null
                }
            }

        val now = OffsetDateTime.now().withNano(0)
        val success = tokenResponse != null
        return HttpResponse
            .ok(
                layout(
                    "refresh",
                    listOf(
                        if (success) {
                            FlashMessage(
                                "Token succesfully refreshed at $now",
                                INFO,
                            )
                        } else {
                            FlashMessage(
                                "Unable to refresh Token at $now",
                                WARNING,
                            )
                        },
                    ),
                    authentication,
                ) {},
//            ).header("HX-Location", originalUri)
            ).cookies(
                mutableListOf<Cookie>()
                    .replaceTokenCookies(
                        accessToken = tokenResponse?.idToken,
                        refreshToken = tokenResponse?.refreshToken,
                    ).toSet(),
            )
    }

    private fun getTokenEndpoint(): SecureEndpoint {
        val authMethodsSupported = openIdProviderMetadata.get()!!.tokenEndpointAuthMethods
        return DefaultSecureEndpoint(openIdProviderMetadata.get()!!.tokenEndpoint, authMethodsSupported.orElse(null))
    }

    class RefreshTokenRequestContext(
        refreshToken: String,
        scope: String,
        tokenEndpoint: SecureEndpoint,
        clientConfiguration: OauthClientConfiguration,
    ) : AbstractTokenRequestContext<MutableMap<String, String>, OpenIdTokenResponse>(
            MediaType.APPLICATION_FORM_URLENCODED_TYPE,
            tokenEndpoint,
            clientConfiguration,
        ) {
        val grant =
            SecureRefreshTokenGrant(
                RefreshTokenGrant().also {
                    it.refreshToken = refreshToken
                    it.scope = scope
                },
            )

        override fun getGrant(): MutableMap<String, String> = grant.toMap()

        override fun getResponseType(): Argument<OpenIdTokenResponse> = Argument.of(OpenIdTokenResponse::class.java)

        override fun getErrorResponseType(): Argument<*> = Argument.of(TokenErrorResponse::class.java)
    }

    class SecureRefreshTokenGrant(
        private val refreshTokenGrant: RefreshTokenGrant,
    ) : AbstractClientSecureGrant() {
        override fun toMap(): MutableMap<String, String> =
            super.toMap().also {
                it["refresh_token"] = refreshTokenGrant.refreshToken
                it["audience"] = "psd2-tpp-dummy"
            }

        override fun getGrantType(): String = refreshTokenGrant.grantType

        override fun setGrantType(grantType: String?): Unit = TODO("Not yet implemented, this should not be needed")
    }
}
