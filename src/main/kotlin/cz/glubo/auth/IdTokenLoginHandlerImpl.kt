package cz.glubo.auth

import cz.glubo.auth.IdTokenLoginHandlerImpl.Companion.COOKIE_NAME_JWT_TOKEN
import cz.glubo.auth.IdTokenLoginHandlerImpl.Companion.COOKIE_NAME_REFRESH_TOKEN
import io.micronaut.context.annotation.Replaces
import io.micronaut.core.annotation.Nullable
import io.micronaut.http.HttpRequest
import io.micronaut.http.MutableHttpResponse
import io.micronaut.http.cookie.Cookie
import io.micronaut.http.cookie.SameSite
import io.micronaut.security.authentication.Authentication
import io.micronaut.security.config.RedirectConfiguration
import io.micronaut.security.config.RedirectService
import io.micronaut.security.errors.PriorToLoginPersistence
import io.micronaut.security.oauth2.endpoint.token.response.IdTokenLoginHandler
import io.micronaut.security.token.cookie.AccessTokenCookieConfiguration
import jakarta.inject.Singleton

@Singleton
@Replaces(IdTokenLoginHandler::class)
class IdTokenLoginHandlerImpl(
    accessTokenCookieConfiguration: AccessTokenCookieConfiguration?,
    redirectConfiguration: RedirectConfiguration?,
    redirectService: RedirectService?,
    priorToLoginPersistence: @Nullable PriorToLoginPersistence<HttpRequest<*>?, MutableHttpResponse<*>?>?,
) : IdTokenLoginHandler(accessTokenCookieConfiguration, redirectConfiguration, redirectService, priorToLoginPersistence) {
    override fun getCookies(
        authentication: Authentication?,
        request: HttpRequest<*>?,
    ): MutableList<Cookie> {
        val refreshToken = authentication?.attributes?.get("refreshToken")?.toString()
        val accessToken = authentication?.attributes?.get("openIdToken")?.toString()

        return super
            .getCookies(authentication, request)
            .replaceTokenCookies(
                accessToken = accessToken,
                refreshToken = refreshToken,
            )
    }

    companion object {
        const val COOKIE_NAME_REFRESH_TOKEN = "REFRESH"
        const val COOKIE_NAME_JWT_TOKEN = "JWT"
    }
}

fun MutableList<Cookie>.replaceTokenCookies(
    accessToken: String?,
    refreshToken: String?,
): MutableList<Cookie> {
    val accessCookie =
        accessToken?.let {
            Cookie
                .of(COOKIE_NAME_JWT_TOKEN, it)
                .path("/")
                .sameSite(SameSite.Lax)
                .httpOnly()
        }

    val refreshCookie =
        refreshToken?.let {
            Cookie
                .of(COOKIE_NAME_REFRESH_TOKEN, it)
                .path("/refresh")
                .sameSite(SameSite.Lax)
                .httpOnly()
        }
    refreshCookie?.let { this.add(it) }
    accessCookie?.let { this.add(it) }

    return this
        .associateBy { it.name }
        .values
        .toMutableList()
}
