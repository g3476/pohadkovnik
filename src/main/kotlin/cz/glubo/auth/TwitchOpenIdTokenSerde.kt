package cz.glubo.auth

import io.micronaut.context.annotation.Replaces
import io.micronaut.core.type.Argument
import io.micronaut.json.tree.JsonArray
import io.micronaut.security.oauth2.endpoint.token.response.OpenIdTokenResponse
import io.micronaut.security.token.DefaultRolesFinder
import io.micronaut.security.token.config.TokenConfiguration
import io.micronaut.serde.Decoder
import io.micronaut.serde.Deserializer
import io.micronaut.serde.Encoder
import io.micronaut.serde.Serde
import io.micronaut.serde.Serializer
import jakarta.inject.Singleton

@Singleton
class TwitchOpenIdTokenSerde : Serde<OpenIdTokenResponse> {
    override fun serialize(
        encoder: Encoder?,
        context: Serializer.EncoderContext?,
        type: Argument<out OpenIdTokenResponse>?,
        value: OpenIdTokenResponse?,
    ) {
        TODO("Not yet implemented")
    }

    override fun deserialize(
        decoder: Decoder,
        context: Deserializer.DecoderContext,
        type: Argument<in OpenIdTokenResponse>?,
    ): OpenIdTokenResponse {
        val node = decoder.decodeNode()
        val scopesNode = node["scope"]
        var scopes =
            if (scopesNode.isArray) {
                val arrayNode: JsonArray = scopesNode as JsonArray
                arrayNode
                    .values()
                    .map { it.stringValue }
                    .toList()
            } else {
                listOf(scopesNode.stringValue)
            }
        val openIdTokenResponse = OpenIdTokenResponse()
        val values = node.entries().associate { it.key to it.value }
        openIdTokenResponse.idToken = values["id_token"]?.stringValue ?: values["access_token"]!!.stringValue
        openIdTokenResponse.accessToken = node["access_token"].stringValue
        openIdTokenResponse.refreshToken = node["refresh_token"].stringValue
        openIdTokenResponse.tokenType = node["token_type"].stringValue
        openIdTokenResponse.setExpiresIn(node["expires_in"].intValue)
        openIdTokenResponse.scope = scopes.joinToString(",")
        return openIdTokenResponse
    }
}

@Replaces(DefaultRolesFinder::class)
@Singleton
class MyRolesFinder(
    tokenConfiguration: TokenConfiguration,
) : DefaultRolesFinder(
        tokenConfiguration,
    ) {
    override fun resolveRoles(attributes: MutableMap<String, Any>): MutableList<String> {
        val originalRoles = super.resolveRoles(attributes)

        val myRoles =
            when {
                attributes["preferred_username"] == "GluboTheMad" -> listOf("ROLE_ADMIN")
                else -> emptyList()
            }
        return originalRoles.plus(myRoles).toMutableList()
    }
}
