package cz.glubo.auth

import cz.glubo.FlashMessage
import cz.glubo.layout
import io.micronaut.context.annotation.Replaces
import io.micronaut.core.annotation.NonNull
import io.micronaut.http.HttpMethod
import io.micronaut.http.MediaType
import io.micronaut.http.MutableHttpResponse
import io.micronaut.http.server.exceptions.response.ErrorContext
import io.micronaut.http.server.exceptions.response.ErrorResponseProcessor
import io.micronaut.http.server.exceptions.response.HateoasErrorResponseProcessor
import io.micronaut.json.JsonConfiguration
import jakarta.inject.Singleton

@Singleton
@Replaces(HateoasErrorResponseProcessor::class)
class HtmxErrorResponseProcessor(
    jacksonConfiguration: JsonConfiguration,
) : ErrorResponseProcessor<String> {
    private val alwaysSerializeErrorsAsList = jacksonConfiguration.isAlwaysSerializeErrorsAsList

    override fun processResponse(
        errorContext: @NonNull ErrorContext?,
        response: @NonNull MutableHttpResponse<*>?,
    ): @NonNull MutableHttpResponse<String>? {
        if (errorContext!!.request.method == HttpMethod.HEAD) {
            return response as MutableHttpResponse<String>?
        }
        val flashMessages =
            when {
                !errorContext.hasErrors() -> {
                    listOf(
                        FlashMessage(
                            response?.reason() ?: "Unknown error",
                            FlashMessage.Type.DANGER,
                        ),
                    )
                }
                else -> {
                    errorContext.errors.map {
                        FlashMessage(
                            it.message,
                            FlashMessage.Type.DANGER,
                        )
                    }
                }
            }

        return response!!
            .body(
                layout(
                    "Error",
                    flashMessages,
                    null,
                    {},
                ),
            ).contentType(MediaType.TEXT_HTML_TYPE)
            .header("HX-Reselect", "#flashMessages")
            .header("HX-Retarget", "#flashMessages")
            .header("HX-Reswap", "afterbegin")
    }
}
