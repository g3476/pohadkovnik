package cz.glubo

import io.micronaut.data.annotation.GeneratedValue
import io.micronaut.data.annotation.Id
import io.micronaut.data.annotation.MappedEntity
import java.math.BigDecimal
import java.time.Instant

@MappedEntity
data class Donation(
    @GeneratedValue
    @Id
    val id: Long,
    val nick: String,
    val amountCzk: BigDecimal,
    val createdAt: Instant,
)
