package cz.glubo

data class FlashMessage(
    val message: String,
    val type: Type,
) {
    enum class Type(
        val cssClass: String,
    ) {
        PRIMARY("alert-primary"),
        SECONDARY("alert-secondary"),
        SUCCESS("alert-success"),
        DANGER("alert-danger"),
        WARNING("alert-warning"),
        INFO("alert-info"),
        LIGHT("alert-light"),
        DARK("alert-dark"),
    }
}
