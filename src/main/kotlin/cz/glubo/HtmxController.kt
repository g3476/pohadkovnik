package cz.glubo

import io.github.oshai.kotlinlogging.KotlinLogging
import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Consumes
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Delete
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.PathVariable
import io.micronaut.http.annotation.Produces
import io.micronaut.security.annotation.Secured
import io.micronaut.security.authentication.Authentication
import io.micronaut.security.rules.SecurityRule
import kotlinx.html.button
import kotlinx.html.div
import kotlinx.html.id
import kotlinx.html.p
import java.net.URI

@Controller(value = "/htmx")
class HtmxController(
    val donationService: DonationService,
) {
    val logger = KotlinLogging.logger { }

    @Secured(SecurityRule.IS_ANONYMOUS)
    @Delete("/donation/{id}")
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    suspend fun deleteDonation(
        @PathVariable
        id: Long,
        authentication: Authentication?,
    ): String =
        hello(
            listOf(
                FlashMessage(
                    "asd $id deleted",
                    FlashMessage.Type.WARNING,
                ),
            ),
            authentication,
        )

    @Secured(SecurityRule.IS_ANONYMOUS)
    @Get("/")
    @Produces(MediaType.TEXT_HTML)
    suspend fun hello(
        flashMessages: Collection<FlashMessage> = emptyList(),
        authentication: Authentication?,
    ): String {
        logger.info { "/" }
        return layout(
            "HTMX",
            flashMessages,
            authentication,
        ) {
            div {
                id = "target"
            }

            button(classes = "btn btn-primary") {
                +"get"
                hxGet("/htmx/get")
                hxTarget("#target")
                hxSelect("#target")
            }
            button(classes = "btn btn-primary") {
                +"get401"
                hxGet("/htmx/get401")
                hxTarget("#target")
                hxSelect("#target")
            }
            button(classes = "btn btn-primary") {
                +"getRed"
                hxGet("/htmx/getRed")
                hxTarget("#target")
                hxSelect("#target")
            }
            button(classes = "btn btn-primary") {
                +"getAuth"
                hxGet("/htmx/getAuth")
                hxTarget("#target")
                hxSelect("#target")
            }
        }
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("/getAuth")
    @Produces(MediaType.TEXT_HTML)
    suspend fun getAuth(authentication: Authentication?) = get(authentication)

    @Secured(SecurityRule.IS_ANONYMOUS)
    @Get("/get")
    @Produces(MediaType.TEXT_HTML)
    suspend fun get(authentication: Authentication?): String {
        logger.info { "get" }
        return layout(
            "HTMX",
            emptyList(),
            authentication,
        ) {
            div {
                id = "target"
                hxSwapOob("true")
                p {
                    +
                        """
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Etiam commodo dui eget wisi. Mauris suscipit, ligula sit amet pharetra semper, nibh ante cursus purus, vel sagittis velit mauris vel metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Praesent id justo in neque elementum ultrices. Praesent in mauris eu tortor porttitor accumsan. Fusce dui leo, imperdiet in, aliquam sit amet, feugiat eu, orci. Aliquam erat volutpat. Aliquam ornare wisi eu metus. Cras elementum. Maecenas lorem. Nunc auctor. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. In rutrum. Duis ante orci, molestie vitae vehicula venenatis, tincidunt ac pede.
                        """.trimIndent()
                }
                p {
                    +
                        """
                        Nulla pulvinar eleifend sem. Sed convallis magna eu sem. Donec quis nibh at felis congue commodo. Integer pellentesque quam vel velit. Nulla quis diam. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. Nullam feugiat, turpis at pulvinar vulputate, erat libero tristique tellus, nec bibendum odio risus sit amet ante. Etiam dictum tincidunt diam. Nullam faucibus mi quis velit. Aliquam ante. Fusce consectetuer risus a nunc. Etiam egestas wisi a erat. Aenean placerat. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Aenean fermentum risus id tortor. Duis pulvinar. Integer lacinia. Aenean vel massa quis mauris vehicula lacinia. Vestibulum erat nulla, ullamcorper nec, rutrum non, nonummy ac, erat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. 
                        """.trimIndent()
                }
            }
        }
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("/get401")
    @Produces(MediaType.TEXT_HTML)
    suspend fun get401(authentication: Authentication?): HttpResponse<String> {
        logger.info { "get" }
        return HttpResponse.unauthorized<String>().body(
            layout(
                "HTMX",
                listOf(
                    FlashMessage(
                        "This should fail 401",
                        FlashMessage.Type.WARNING,
                    ),
                ),
                authentication,
            ) {
                div {
                    id = "target"
                    hxSwapOob("true")
                    p {
                        +
                            """
                            Nulla pulvinar eleifend sem. Sed convallis magna eu sem. Donec quis nibh at felis congue commodo. Integer pellentesque quam vel velit. Nulla quis diam. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. Nullam feugiat, turpis at pulvinar vulputate, erat libero tristique tellus, nec bibendum odio risus sit amet ante. Etiam dictum tincidunt diam. Nullam faucibus mi quis velit. Aliquam ante. Fusce consectetuer risus a nunc. Etiam egestas wisi a erat. Aenean placerat. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Aenean fermentum risus id tortor. Duis pulvinar. Integer lacinia. Aenean vel massa quis mauris vehicula lacinia. Vestibulum erat nulla, ullamcorper nec, rutrum non, nonummy ac, erat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. 
                            """.trimIndent()
                    }
                }
            },
        )
    }

    @Secured(SecurityRule.IS_ANONYMOUS)
    @Get("/getRed")
    @Produces(MediaType.TEXT_HTML)
    suspend fun getRed(authentication: Authentication?): HttpResponse<String> {
        logger.info { "get" }
        return HttpResponse
            .unauthorized<String>()
            .body(
                layout(
                    "HTMX",
                    listOf(
                        FlashMessage(
                            "This should fail Red",
                            FlashMessage.Type.WARNING,
                        ),
                    ),
                    authentication,
                ) {
                    div {
                        id = "target"
                        hxSwapOob("true")
                        p {
                            +
                                """
                                ASDljkansldkjans askljdnaslkjdnalskjask  kj naslkdnals ksad lkasjn laskj d
                                """.trimIndent()
                        }
                    }
                },
            ).headers {
                it.location(URI("/htmx/get"))
                it["Hx-Location"] = "/htmx/get"
            }
    }
}
