package cz.glubo

import jakarta.inject.Singleton
import kotlinx.coroutines.flow.toList
import java.math.BigDecimal
import java.time.Instant

@Singleton
class DonationService(
    val donationRepository: DonationRepository,
) {
    suspend fun getAll() = donationRepository.findAll().toList()

    suspend fun getTop(): List<DonationRepository.TopDonation> = donationRepository.topDonators()

    suspend fun getLast(): Donation = donationRepository.getLast()

    suspend fun getSum(): BigDecimal = donationRepository.sumDonations()

    suspend fun getUnique(): Long = donationRepository.sumUniqueDonators()

    suspend fun newDonation(
        nick: String,
        amountCzk: BigDecimal,
    ) = donationRepository.save(
        Donation(
            id = 0,
            nick = nick,
            amountCzk = amountCzk,
            createdAt = Instant.now(),
        ),
    )

    suspend fun deleteDonation(id: Long): Donation {
        val ret = donationRepository.findById(id) ?: throw RuntimeException("Donation $id not found")
        donationRepository.deleteById(id)
        return ret
    }
}
