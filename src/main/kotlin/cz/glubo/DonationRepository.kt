package cz.glubo

import io.micronaut.data.annotation.MappedEntity
import io.micronaut.data.annotation.Query
import io.micronaut.data.model.query.builder.sql.Dialect
import io.micronaut.data.r2dbc.annotation.R2dbcRepository
import io.micronaut.data.repository.kotlin.CoroutineCrudRepository
import java.math.BigDecimal

@R2dbcRepository(dialect = Dialect.MYSQL)
interface DonationRepository : CoroutineCrudRepository<Donation, Long> {
    @Query(
        """
            SELECT nick, SUM(amount_czk) as total_amount_czk
            FROM `donation`
            GROUP BY nick
            ORDER BY total_amount_czk DESC
            LIMIT 5
        """,
        nativeQuery = true,
    )
    suspend fun topDonators(): List<TopDonation>

    @Query(
        """
            SELECT SUM(amount_czk) as total_amount_czk
            FROM `donation`
        """,
        nativeQuery = true,
    )
    suspend fun sumDonations(): BigDecimal

    @Query(
        """
            SELECT COUNT(DISTINCT (nick))
            FROM `donation`
        """,
        nativeQuery = true,
    )
    suspend fun sumUniqueDonators(): Long

    @Query(
        """
            SELECT id, nick, amount_czk, created_at
            FROM `donation`
            ORDER BY created_at DESC
            LIMIT 1
        """,
        nativeQuery = true,
    )
    suspend fun getLast(): Donation

    @MappedEntity
    data class TopDonation(
        val nick: String,
        val totalAmountCzk: BigDecimal,
    )
}
