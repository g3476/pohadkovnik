package cz.glubo

import com.github.doyaaaaaken.kotlincsv.dsl.csvWriter
import io.github.oshai.kotlinlogging.KotlinLogging
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Consumes
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Delete
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.PathVariable
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.Produces
import io.micronaut.security.annotation.Secured
import io.micronaut.security.authentication.Authentication
import io.micronaut.security.rules.SecurityRule
import io.micronaut.serde.annotation.Serdeable
import kotlinx.html.ButtonType
import kotlinx.html.FormMethod
import kotlinx.html.InputType
import kotlinx.html.button
import kotlinx.html.div
import kotlinx.html.form
import kotlinx.html.h2
import kotlinx.html.id
import kotlinx.html.input
import kotlinx.html.label
import kotlinx.html.span
import kotlinx.html.style
import kotlinx.html.table
import kotlinx.html.tbody
import kotlinx.html.td
import kotlinx.html.th
import kotlinx.html.thead
import kotlinx.html.tr
import java.math.BigDecimal
import java.text.NumberFormat
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter

val numberFormat = NumberFormat.getNumberInstance(java.util.Locale.of("cs"))

fun BigDecimal.toHumanNumber() = numberFormat.format(this)

@Controller
class HelloController(
    val donationService: DonationService,
) {
    val logger = KotlinLogging.logger { }

    @Secured(SecurityRule.IS_ANONYMOUS)
    @Delete("/donation/{id}")
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    suspend fun deleteDonation(
        @PathVariable
        id: Long,
        authentication: Authentication?,
    ): String =
        donationService
            .deleteDonation(
                id,
            ).let { deletedDonation ->
                hello(
                    listOf(
                        FlashMessage(
                            "Donation $deletedDonation deleted",
                            FlashMessage.Type.WARNING,
                        ),
                    ),
                    authentication,
                )
            }

    @Secured(SecurityRule.IS_ANONYMOUS)
    @Post("/donation")
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    suspend fun newDonation(
        @Body
        body: NewDonationForm,
        authentication: Authentication?,
    ): String =
        donationService
            .newDonation(
                nick = body.nick,
                amountCzk = body.amountCzk,
            ).let { savedDonation ->
                hello(
                    listOf(
                        FlashMessage(
                            "Donation $savedDonation Added",
                            FlashMessage.Type.SUCCESS,
                        ),
                    ),
                    authentication,
                )
            }

    @Serdeable
    data class NewDonationForm(
        val nick: String,
        val amountCzk: BigDecimal,
    )

    @Secured(SecurityRule.IS_ANONYMOUS)
    @Get("/")
    @Produces(MediaType.TEXT_HTML)
    suspend fun hello(
        flashMessages: Collection<FlashMessage> = emptyList(),
        authentication: Authentication?,
    ): String {
        val donations = donationService.getAll()
        val topDonations = donationService.getTop()
        return layout(
            "Pohadkovnik",
            flashMessages,
            authentication,
        ) {
            form(action = "/donation", method = FormMethod.post, classes = "") {
                div(classes = "row g-4 align-items-center") {
                    div(classes = "col-auto") {
                        label("col-form-label") {
                            htmlFor = "inputNickname"
                            +"Nickname"
                        }
                        input(InputType.text, name = "nick", classes = "form-control") {
                            id = "inputNickname"
                        }
                    }
                    div(classes = "col-auto") {
                        label("col-form-label") {
                            htmlFor = "inputAmountCzk"
                            +"Částka [Kč]"
                        }
                        input(InputType.number, name = "amountCzk", classes = "form-control") {
                            step = "0.01"
                            id = "inputAmountCzk"
                        }
                    }
                }
                button(
                    classes = "btn btn-primary",
                    type = ButtonType.submit,
                    name = "save",
                ) {
                    +"Ulož"
                    this.attributes["hx-post"] = "/donation"
                    this.attributes["hx-select"] = "#content"
                    this.attributes["hx-target"] = "#content"
                    this.attributes["hx-disabled-elt"] = "this"
                }
            }
            div {
                id = "dataTable"
                this.attributes["hx-get"] = "/"
                this.attributes["hx-trigger"] = "every 2s"
                this.attributes["hx-select"] = "#dataTable"
                this.attributes["hx-target"] = "#dataTable"
                h2 { +"Top donates" }
                table("table") {
                    thead {
                        tr {
                            th {
                                +"Nick"
                            }
                            th {
                                +"Částka [Kč]"
                            }
                        }
                    }
                    tbody {
                        topDonations.forEach { donation ->
                            tr {
                                td {
                                    +donation.nick
                                }
                                td {
                                    +donation.totalAmountCzk.toHumanNumber()
                                }
                            }
                        }
                    }
                }
                h2 { +"Všechny donates" }
                table("table") {
                    thead {
                        tr {
                            th {
                                +"Id"
                            }
                            th {
                                +"Nick"
                            }
                            th {
                                +"Částka [Kč]"
                            }
                            th {
                                +"Zadáno poprvé"
                            }
                            th {
                                +"Akce"
                            }
                        }
                    }
                    tbody {
                        donations.forEach { donation ->
                            tr {
                                td {
                                    +donation.id.toString()
                                }
                                td {
                                    +donation.nick
                                }
                                td {
                                    +donation.amountCzk.toHumanNumber()
                                }
                                td {
                                    +donation.createdAt.toHumanString()
                                }
                                td {
                                    button(
                                        classes = "btn btn-danger",
                                        type = ButtonType.submit,
                                        name = "delete-${donation.id}",
                                    ) {
                                        +"Smaž"
                                        attributes["hx-delete"] = "/donation/${donation.id}"
                                        attributes["hx-select"] = "#content"
                                        attributes["hx-target"] = "#content"
                                        attributes["hx-confirm"] =
                                            "Really Delete donation #${donation.id} from ${donation.nick}, ${donation.amountCzk.toHumanNumber()} Kč from ${donation.createdAt.toHumanString()}?"
                                        attributes["hx-disabled-elt"] = "this"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Secured(SecurityRule.IS_ANONYMOUS)
    @Get("/obs-top")
    @Produces(MediaType.TEXT_HTML)
    suspend fun obsTop() =
        donationService.getTop().let { topDonos ->
            layoutRaw("Top Donations") {
                this.div("container-fluid d-flex justify-content-center align-items-center") {
                    this.style = "height:100vh; width:100vw;overflow:hidden"
                    this.div("row text-center d-flex align-items-center") {
                        this.style = "width:100vw"
                        this.attributes["hx-get"] = "/obs-top"
                        this.attributes["hx-trigger"] = "every 1s"
                        topDonos.forEach {
                            this.div("row text-center d-flex align-items-center") {
                                this.style = "width:100vw"
                                span {
                                    this.style = "width:100vw"
                                    +"${it.nick}: ${it.totalAmountCzk.toHumanNumber()} Kč "
                                }
                            }
                        }
                    }
                }
            }
        }

    @Secured(SecurityRule.IS_ANONYMOUS)
    @Get("/obs-sum")
    @Produces(MediaType.TEXT_HTML)
    suspend fun obsSum(): String {
        val sum = donationService.getSum()
        val last = donationService.getLast()
        val unique = donationService.getUnique()

        return layoutRaw("Sum of Donations") {
            this.div("container-fluid d-flex justify-content-center align-items-center") {
                this.style = "height:100vh; width:100vw;overflow:hidden"
                this.div("row text-center d-flex align-items-center") {
                    this.style = "width:100vw"
                    this.attributes["hx-get"] = "/obs-sum"
                    this.attributes["hx-trigger"] = "every 1s"
                    this.div("row text-center d-flex align-items-center") {
                        this.style = "width:100vw"
                        span {
                            this.style = "width:100vw"
                            +"Celkem: ${sum.toHumanNumber()} Kč (od $unique lidí)"
                        }
                        span {
                            this.style = "width:100vw;font-size:60%"
                            +"Poslední: ${last.nick} ${last.amountCzk.toHumanNumber()} Kč "
                        }
                    }
                }
            }
        }
    }

    @Secured(SecurityRule.IS_ANONYMOUS)
    @Get("/csv")
    @Produces(MediaType.TEXT_CSV)
    suspend fun csv() =
        donationService
            .getAll()
            .map { donation ->
                listOf(donation.id, donation.nick, donation.amountCzk, donation.createdAt)
            }.let {
                csvWriter().writeAllAsString(it)
            }
}

fun Instant.toHumanString(): String =
    this
        .atZone(ZoneId.of("Europe/Prague"))
        .toLocalDateTime()
        .format(DateTimeFormatter.ofPattern("d.M. HH:mm:ss"))
