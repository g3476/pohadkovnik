package cz.glubo

import io.github.oshai.kotlinlogging.KotlinLogging
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Produces
import io.micronaut.http.exceptions.HttpException
import io.micronaut.http.server.exceptions.ExceptionHandler

// @Singleton
@Produces
class ErrorHandler : ExceptionHandler<Throwable, HttpResponse<Any>> {
    val logger = KotlinLogging.logger { }

    override fun handle(
        request: HttpRequest<*>,
        exception: Throwable?,
    ): HttpResponse<Any> {
        when (exception) {
            is HttpException -> logger.debug { exception }
            else -> logger.warn { exception }
        }
        return HttpResponse
            .ok(
                layout(
                    "Error",
                    listOf(
                        FlashMessage(
                            exception?.message ?: "Unknown error",
                            FlashMessage.Type.DANGER,
                        ),
                    ),
                    null,
                ) {},
            ).contentType(MediaType.TEXT_HTML_TYPE)
            .header("HX-Reselect", "#flashMessages")
            .header("HX-Retarget", "#flashMessages")
            .header("HX-Reswap", "afterbegin")
            as HttpResponse<Any>
    }
}
