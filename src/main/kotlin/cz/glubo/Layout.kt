package cz.glubo

import io.micronaut.security.authentication.Authentication
import kotlinx.html.BODY
import kotlinx.html.ButtonType
import kotlinx.html.DIV
import kotlinx.html.Entities
import kotlinx.html.ScriptCrossorigin
import kotlinx.html.a
import kotlinx.html.body
import kotlinx.html.button
import kotlinx.html.div
import kotlinx.html.dom.createHTMLDocument
import kotlinx.html.dom.serialize
import kotlinx.html.h1
import kotlinx.html.head
import kotlinx.html.html
import kotlinx.html.id
import kotlinx.html.link
import kotlinx.html.meta
import kotlinx.html.nav
import kotlinx.html.p
import kotlinx.html.script
import kotlinx.html.span
import kotlinx.html.title
import kotlinx.html.unsafe

fun Authentication?.findUserName() =
    this?.attributes?.get("preferred_username")
        ?: this?.name
        ?: "Anonymouus"

fun layout(
    title: String,
    flashMessages: Collection<FlashMessage> = emptyList(),
    authentication: Authentication?,
    content: DIV.() -> Unit,
): String =
    layoutRaw(title) {
        nav("navbar navbar-expand-lg navbar-dark bg-dark") {
            id = "main-navbar"
            hxSwapOob("true")
            div("container-fluid") {
                a(href = "/", classes = "navbar-brand") {
                    +"Pohadkovnik"
                }
                a(href = "/htmx", classes = "navbar-item") {
                    +"Htmx"
                }

                if (authentication != null) {
                    span("navbar-brand") { +"${authentication.findUserName()}" }
                    a(href = "/logout") {
                        +"logout"
                    }
                } else {
                    a(href = "/oauth/login/twitch") {
//                    <img src="/static/user.png" alt="User icon" width="32" height="32" class="d-inline-block align-text-top">
                        +"login"
                    }
                }
            }
        }
        div("container-fluid") {
            h1 { +title }
            div {
                id = "flashMessages"
                attributes["hx-swap-oob"] = "afterbegin"
                flashMessages.forEach {
                    div("alert ${it.type.cssClass} alert-dismissible fade show") {
                        button(
                            type = ButtonType.button,
                            classes = "btn-close",
                        ) {
                            attributes["data-bs-dismiss"] = "alert"
                            attributes["aria-label"] = "Close"
                            span {
                                attributes["aria-hidden"] = "true"
                                +Entities.nbsp
                            }
                        }
                        attributes["role"] = "alert"
                        p {
                            +it.message
                        }
                    }
                }
            }
            div {
                id = "content"
                content()
            }
        }
    }

fun layoutRaw(
    title: String,
    body: BODY.() -> Unit,
) = createHTMLDocument()
    .html {
        head {
            title {
                +"Pohadkovnik: $title"
            }
            script {
                src = "https://unpkg.com/htmx.org@2.0.0"
                integrity = "sha384-wS5l5IKJBvK6sPTKa2WZ1js3d947pvWXbPJ1OmWfEuxLgeHcEbjUUA5i9V5ZkpCw"
                crossorigin = ScriptCrossorigin.anonymous
            }
            link {
                href = "https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css"
                rel = "stylesheet"
                integrity = "sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH"
                attributes["crossorigin"] = "anonymous"
            }
            script {
                src = "https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"
                integrity = "sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz"
                crossorigin = ScriptCrossorigin.anonymous
            }
            meta(
                name = "htmx-config",
                content =
                    """
                    {"responseHandling": [
                        {"code":"401", "swap": true, "error": true},
                        {"code":".*", "swap": true}
                    ]}
                    """.trimIndent(),
            )
        }

        body {
            body()
            unsafe {
                +
                    """
                    <script>
                    htmx.logAll();
                        document.body.addEventListener('htmx:responseError', function(event) {
                          if (event.detail.xhr.status === 401) {
                            // Refresh the session
                            fetch('/refresh', {
                              method: 'GET',
                            })
                            .then(response => {
                            console.log("A");
                              if (!response.ok) {
                                console.log("B");
                                throw new Error('Network response was not ok');
                              }
                                console.log("C");
                              // Retry the original request
                                console.log(event);
                              // htmx.trigger(event.target, 'htmx:confirm');
                              const eventType = event.detail.requestConfig.triggeringEvent.type
                               htmx.trigger(event.detail.elt, eventType)
                                console.log("CC");
                            })
                            .catch(error => {
                                console.log("D");
                              throw new Error(`There has been a problem with refreshing the session: ${'$'}{error}`);
                            });
                          }
                        });
                    </script>
                    """.trimIndent()
            }
        }
    }.serialize()
