package cz.glubo

import io.kotest.core.spec.style.FreeSpec
import io.kotest.data.Row2
import io.kotest.data.forAll
import io.kotest.matchers.shouldBe

class BigDecimatFormatTest :
    FreeSpec({
        "Human Format for BigDecimal" {
            forAll(
                Row2("0", "0"),
                Row2("1", "1"),
                Row2("100", "100"),
                Row2("1000", "1 000"),
                Row2("1000000", "1 000 000"),
                Row2("10.00", "10"),
                Row2("10.04", "10,04"),
            ) { a, b ->
                val input = a.toBigDecimal()
                input.toHumanNumber() shouldBe b
            }
        }
    })
