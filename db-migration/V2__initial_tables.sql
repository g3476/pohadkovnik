CREATE TABLE IF NOT EXISTS `donation`
(
    `id`         bigint(20)     NOT NULL AUTO_INCREMENT,
    `nick`       tinytext       NOT NULL,
    `created_at` timestamp      NOT NULL,
    `amount_czk` decimal(10, 2) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `nick` (`nick`(255))
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;
